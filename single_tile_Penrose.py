# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 18:03:40 2016

@author: nicolas

Generate a single Penrose tile in svg format, to test compatibility with Trotec
"""

import cairo
import numpy as np

def invert(cr, height_in_points):
    """
    When a context cr is created, the origin is at the upper left corner and the y axis is pointing downwards.
    Assuming that no transformation has been performed on cr, 
    this function inverts the y axis and places the origin and the lower left corner of the image.
    """
    cr.scale(1., -1.)
    cr.translate(0., -height_in_points)
    return None

def init(fobj, width_in_points, height_in_points):
    """
    Return a Cairo surface and a Cairo context
    fobj: file to draw in (can be a filename or a file object)
    width_in_points/height_in_points: 1 point == 1/72.0 inch == 0.3527... mm
    """
    # create an SVG cairo surface
    surface = cairo.SVGSurface(fobj, width_in_points, height_in_points)
    # default cairo context
    cr = cairo.Context(surface)
    
    # invert the y axis and set the origin to the lower left corner
    invert(cr, height_in_points)
    
    return surface, cr
    
def dimensions(cr):
    """
    Return the image dimensions in milimeters, for the context cr
    """
    m = cr.get_matrix() # entries of this matrix are in pts
    mmPerPts = 25.4/72. # 1 pt = 25.4/72 mm
    width_in_mm = abs(m[0])*mmPerPts
    height_in_mm = abs(m[3])*mmPerPts
    return width_in_mm, height_in_mm
    
def unpack(cmplx):
    return cmplx.real, cmplx.imag
    
def rgb(colorstring):
    """
    convert an html-coded color to the format used by Cairo
    """
    colorstring = colorstring.strip()
    if colorstring[0] == '#': colorstring = colorstring[1:]
    if len(colorstring) != 6:
        raise(ValueError, "input #%s is not in #RRGGBB format" % colorstring)
    r, g, b = colorstring[:2], colorstring[2:4], colorstring[4:]
    r, g, b = [int(n, 16) for n in (r, g, b)]
    return (r, g, b)

om = (np.sqrt(5)-1)/2 # inverse of the golden ratio
pi = np.pi

width_in_points, height_in_points = 100, 100
surface, cr = init("Penrose_Tile.svg", width_in_points, height_in_points)
# rescale the image so that the new length is L
L = 1 + om/2 # horizontal length of the fat rhombus
s = width_in_points/L
cr.scale(s, s)

# the four vertices of the fat rhombus (running counterclockwise)
A = 0j
B = 0j + 1.
C = B + np.exp(1j*2*pi/5)
D = np.exp(1j*2*pi/5)

"""
Draw the tile edges
"""
# line width is alpha smaller than the edge length
edge_length = 1.
alpha = 100.
cr.set_line_width(edge_length/alpha)
line_width_in_mm = 0.1
cr.set_line_width(line_width_in_mm*72/25.4/s)

# draw a closed path between the four vertices of the rhombus
cr.move_to(*unpack(A))
cr.line_to(*unpack(B))
cr.line_to(*unpack(C))
cr.line_to(*unpack(D))
cr.close_path()
# line color
green = "#00FF00" # green: traits de découpe
cr.set_source_rgb(*rgb(green))
cr.stroke()

"""
Draw the tile decorations
"""
D = 0.8*edge_length # distance 
d = edge_length - D

# circle color
red = "#FF0000" # red: traits de marquage
cr.set_source_rgb(*rgb(red))

# big decoration
cr.arc(*unpack(C), D, pi, -3*pi/5)
cr.stroke()

# small decoration: we draw two concentric arcs to differentiate it from the big decoration
cr.arc(*unpack(A), d, 0, 2*pi/5)
delr = 0.05*edge_length # the distance between the two concentric arcs
cr.new_sub_path() # prevents drawing the path between first and second arc
cr.arc(*unpack(A), d + delr, 0, 2*pi/5)
cr.stroke()

surface.finish()
