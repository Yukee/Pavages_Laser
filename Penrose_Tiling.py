# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 16:58:26 2016

@author: nicolas

Output an svg and png image of a Penrose tiling, with decorated tiles.
Remark: this script needs the Tilings package to run. It can be found at https://framagit.org/Yukee/gaps_topology
"""

import sys
sys.path.insert(0, '../gaps_topology/Tilings') # prepend the path to the Tiling package
import Tilings as tl

import numpy as np

# Create a fivefold flower of fat rhombuses
shapes0 = []
for i in range(5):
    C = 0j
    A = np.exp(1j*2*i*np.pi/5)
    B = np.exp(1j*2*i*np.pi/5)+np.exp(1j*2*(i+1)*np.pi/5)
    shapes0.append((1 , (A, B, C)))
    A = np.exp(1j*2*(i+1)*np.pi/5)
    shapes0.append((1 , (A, B, C)))

P = tl.Penrose(shapes0)
nsub = 5
P.it_sub(nsub)

s = 2.4
P.draw((500,500), s, (s/2, s/2), "Penrose_Tiling", draw_decorations=True)
P.save_image("Penrose_Tiling")