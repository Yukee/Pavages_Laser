# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 15:21:27 2016

@author: nicolas

Ce script fait joujou avec les outils de PyCairo
"""

import cairo
import numpy as np

def invert(cr, height_in_points):
    """
    When a context cr is created, the origin is at the upper left corner and the y axis is pointing downwards.
    Assuming that no transformation has been performed on cr, 
    this function inverts the y axis and places the origin and the lower left corner of the image.
    """
    cr.scale(1., -1.)
    cr.translate(0., -height_in_points)
    return None

def init(fobj, width_in_points, height_in_points):
    """
    Return a Cairo surface and a Cairo context
    fobj: file to draw in (can be a filename or a file object)
    width_in_points/height_in_points: 1 point == 1/72.0 inch == 0.3527... mm
    """
    # create an SVG cairo surface
    surface = cairo.SVGSurface(fobj, width_in_points, height_in_points)
    # default cairo context
    cr = cairo.Context(surface)
    
    # invert the y axis and set the origin to the lower left corner
    invert(cr, height_in_points)
    
    return surface, cr
    
"""
A minimal example showing how to create and draw on an svg file
"""
width_in_points, height_in_points = 100, 100
surface, cr = init("Hello_World.svg", width_in_points, height_in_points) # create an svg file, a surface and a context
invert(cr, height_in_points) # invert twice (ie don't invert) to prevent text from being flipped upside-down
cr.select_font_face("Helvetica", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL) # font options
cr.set_font_size(15)
cr.move_to(10, 60) # move the drawing head (it starts at the upper left corner of the image)
cr.show_text("Hello world!") # draw some text
surface.finish() # disconnect the surface from the interface drawing on the svg file

"""
Draw a fat rhombus of the Penrose P3 tiling
"""
om = (np.sqrt(5)-1)/2 # inverse of the golden ratio
pi = np.pi

surface, cr = init("Penrose_Fat_Rhombus.svg", width_in_points, height_in_points)
# rescale the image so that the new length is L
L = 1 + om/2 # horizontal length of the fat rhombus
s = width_in_points/L
cr.scale(s, s)

# the four vertices of the fat rhombus (running counterclockwise)
A = 0j
B = 0j + 1.
C = B + np.exp(1j*2*pi/5)
D = np.exp(1j*2*pi/5)

def unpack(cmplx):
    return cmplx.real, cmplx.imag

# draw a closed path between the four vertices of the rhombus
cr.move_to(*unpack(A))
cr.line_to(*unpack(B))
cr.line_to(*unpack(C))
cr.line_to(*unpack(D))
cr.close_path()
# fill with a color
lightred = (0.697, 0.425, 0.333)
cr.set_source_rgb(*lightred)
cr.fill()

surface.finish()

"""
Draw a thin rhombus of the Penrose P3 tiling
"""
surface, cr = init("Penrose_Thin_Rhombus.svg", width_in_points, height_in_points)
# rescale the image so that the new length is L
L = 1 + 1/(2*om) # horizontal length of the fat rhombus
s = width_in_points/L
cr.scale(s, s)

# the four vertices of the thin rhombus (running counterclockwise)
A = 0j
B = 0j + 1.
C = B + np.exp(1j*pi/5)
D = np.exp(1j*pi/5)

# draw a closed path between the four vertices of the rhombus
cr.move_to(*unpack(A))
cr.line_to(*unpack(B))
cr.line_to(*unpack(C))
cr.line_to(*unpack(D))
cr.close_path()
# fill with a color
lightblue = (0.333, 0.605, 0.697)
cr.set_source_rgb(*lightblue)
cr.fill()

surface.finish()
